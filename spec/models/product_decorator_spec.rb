require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  describe "related_productsを使用する" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }

    let!(:related_product) { create(:product, taxons: [taxon]) }

    let!(:not_related_product) { create(:product) }

    it "商品(product)と同じカテゴリーに属する関連商品(related_product)が表示されること" do
      expect(product.related_products).to include related_product
    end

    it "関連商品に商品が重複して表示されないこと" do
      expect(product.related_products).not_to include product
    end

    it "商品と別のカテゴリーに属する関連商品は表示されないこと" do
      expect(product.related_products).not_to include not_related_product
    end
  end
end
