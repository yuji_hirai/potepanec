require 'rails_helper'

RSpec.feature "Categories" do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }

  let(:sample_taxonomy) { create(:taxonomy, name: "sample_taxonomy_name") }
  let(:sample_taxon) { create(:taxon, name: "sample_taxon_name", taxonomy: sample_taxonomy) }
  let!(:sample_product) { create(:product, name: "sample_product_name", price: "111", taxons: [sample_taxon]) }

  feature "リンク先に移動" do
    background do
      visit potepan_category_path(taxon.id)
    end

    scenario "page_titleのHOMEは、トップページへのリンクであること" do
      within(".breadcrumb") do
        expect(page).to have_link 'HOME', href: potepan_path
      end
    end

    scenario "navbarのHOMEは、トップページへのリンクであること" do
      within(".navbar-right") do
        expect(page).to have_link 'HOME', href: potepan_path
      end
    end

    scenario "navbarのBIG_BAG画像は、トップページへのリンクであること" do
      within(".navbar-header") do
        expect(page).to have_link nil, href: potepan_path
      end
    end

    scenario "商品名は、商品詳細ページへのリンクであること" do
      expect(page).to have_link product.name, href: potepan_product_path(product.id)
    end

    scenario "商品価格は、商品詳細ページへのリンクであること" do
      expect(page).to have_link product.display_price, href: potepan_product_path(product.id)
    end
  end

  context "taxonに紐付いた商品(product)の場合" do
    background do
      visit potepan_category_path(taxon.id)
    end

    scenario "taxon名をクリックすると、商品が属するカテゴリーの商品一覧ページに移動すること" do
      click_link taxon.name
      expect(current_path).to eq potepan_category_path(taxon.id)
    end

    scenario "taxon名をクリックすると、商品一覧ページに商品名が表示されること" do
      click_link taxon.name
      expect(page).to have_content product.name
    end

    scenario "taxon名をクリックすると、商品一覧ページに商品価格が表示されること" do
      click_link taxon.name
      expect(page).to have_content product.price.to_s
    end
  end

  context "taxonに紐付いていない商品(sample_product)の場合" do
    background do
      visit potepan_category_path(taxon.id)
    end

    scenario "taxon名をクリックすると、商品が属するカテゴリーの商品一覧ページに移動しないこと" do
      click_link taxon.name
      expect(current_path).not_to eq potepan_category_path(sample_taxon.id)
    end

    scenario "taxon名をクリックすると、商品一覧ページに商品名は表示されないこと" do
      click_link taxon.name
      expect(page).not_to have_content "sample_product_name"
    end

    scenario "taxon名をクリックすると、商品一覧ページに商品価格は表示されないこと" do
      click_link taxon.name
      expect(page).not_to have_content "111"
    end
  end
end
