require 'rails_helper'

RSpec.feature "Products" do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:product) { create(:product, taxons: [taxon]) }

  let!(:related_product) { create(:product, name: "related_product_name_1", taxons: [taxon]) }

  background do
    visit potepan_product_path(product.id)
  end

  feature "リンク先に移動する" do
    scenario "page_headerのHOMEは、トップページへのリンクであること" do
      within(".breadcrumb") do
        expect(page).to have_link 'HOME', href: potepan_path
      end
    end

    scenario "navbarのHOMEは、トップページへのリンクであること" do
      within(".navbar-right") do
        expect(page).to have_link 'HOME', href: potepan_path
      end
    end

    scenario "navbarのBIG_BAG画像は、トップページへのリンクであること" do
      within(".navbar-header") do
        expect(page).to have_link nil, href: potepan_path
      end
    end

    scenario "「一覧ページへ戻る」は、商品が属するカテゴリー一覧へのリンクであること" do
      expect(page).to have_link '一覧ページへ戻る', href: potepan_category_path(taxon.id)
    end

    scenario "関連商品は、同商品の詳細ページへのリンクであること" do
      within(".related_product") do
        expect(page).to have_link related_product.name, href: potepan_product_path(related_product.id)
        expect(page).to have_link related_product.price.to_s, href: potepan_product_path(related_product.id)
      end
    end
  end
end
