require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe "#show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }

    let!(:related_products) { create_list(:product, 5, name: "related_product_name", price: "111", taxons: [taxon]) }

    let(:not_related_taxonomy) { create(:taxonomy) }
    let(:not_related_taxon) { create(:taxon, taxonomy: not_related_taxonomy) }
    let!(:not_related_product) { create(:product, name: "not_related_product_name", price: "999", taxons: [not_related_taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it "showページへのレスポンスが成功されること" do
      expect(response).to be_successful
    end

    it "タイトルは、商品名 - BIG BAG Storeが表示されること" do
      expect(response.body).to include "#{product.name} - BIG BAG Store"
    end

    it "商品名が表示されること" do
      expect(response.body).to include product.name
    end

    it "商品価格が表示されること" do
      expect(response.body).to include product.price.to_s
    end

    it "商品の詳細が表示されること" do
      expect(response.body).to include product.description
    end

    it "商品画像が表示されること" do
      product.images.each do |image|
        expect(response.body).to include image.attachment.call(:large)
        expect(response.body).to include image.attachment.call(:small)
      end
    end

    context "商品詳細ページの商品と同じのカテゴリーに属する関連商品の場合" do
      it "関連商品に商品名が表示されていること" do
        expect(response.body).to include "related_product_name"
      end

      it "関連商品に商品価格が表示されていること" do
        expect(response.body).to include "111"
      end

      it "関連商品は、最大4つまで表示されること" do
        expect(controller.instance_variable_get("@related_products").length).to eq 4
      end
    end

    context "商品詳細ページの商品とは別のカテゴリーに属する関連していない商品の場合" do
      it "関連商品に商品名が表示されていない" do
        expect(response.body).not_to include "not_related_product_name"
      end

      it "関連商品に商品価格が表示されていないこと" do
        expect(response.body).not_to include "999"
      end
    end
  end
end
