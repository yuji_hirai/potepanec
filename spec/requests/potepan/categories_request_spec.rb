require 'rails_helper'

RSpec.describe "Categories", type: :request do
  describe "#show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }

    let(:sample_taxonomy) { create(:taxonomy, name: "sample_taxonomy_name") }
    let(:sample_taxon) { create(:taxon, name: "sample_taxon_name", taxonomy: sample_taxonomy) }
    let!(:sample_product) { create(:product, name: "sample_product_name", price: "111", taxons: [sample_taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "showページへのレスポンスが成功すること" do
      expect(response).to be_successful
    end

    it "タイトルは、taxon名 - BIG BAG Storeが表示されること" do
      expect(response.body).to include "#{taxon.name} - BIG BAG Store"
    end

    it "サイドバーの商品カテゴリー大項目にtaxonomy名が表示されていること" do
      expect(response.body).to include taxonomy.name
    end

    it "サイドバーの商品カテゴリー小項目にtaxons名が表示されていること" do
      expect(response.body).to include taxon.name
    end

    it "サイドバーのカテゴリー小項目にtaxonsと紐付いている商品総数が表示されていること" do
      expect(response.body).to include taxon.products.size.to_s
    end

    context "taxonsと商品が紐付いている場合" do
      it "商品名が表示されていること" do
        expect(response.body).to include product.name
      end

      it "商品価格が表示されていること" do
        expect(response.body).to include product.price.to_s
      end
    end

    context "taxonsと商品が紐付いていない場合" do
      it "商品名は表示されていないこと" do
        expect(response.body).not_to include "sample_product_name"
      end

      it "商品価格は表示されていないこと" do
        expect(response.body).not_to include "111"
      end
    end
  end
end
