class Potepan::ProductsController < ApplicationController
  MAXIMUM_NUMBER_OF_RELATED_PRODUCTS = 4

  def show
    @product = Spree::Product.find(params[:id])
    @taxons = @product.taxons.first
    @related_products = @product.related_products.sample(MAXIMUM_NUMBER_OF_RELATED_PRODUCTS)
  end
end
